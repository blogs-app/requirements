## Blogging Application

### Requirements

- User Authentication
    - registration 
    - login
    - forgot password
    - reset password
    - user blocking
- Blogs
    - authenticated user
        - blog actions
            - add blog
            - edit blog
            - delete blog
            - make a blog public/private
            - like/dislike a blog
        - comment actions
            - add a comment
            - edit a comment
            - delete a comment
        - rating actions
            - add a rating
            - edit a rating
            - delete a rating
    - general user blog actions
        - search blogs
            - by tags
            - by user
            - by date
            - by popularity
        - share a blog

### Database

- db name : blogs_db
- tables : 
    - users
    - blogs
    - attachments
    - comments
    - ratings
    - likes

### Backend

- user apis
    - POST /signup
    - POST /signin
    - POST /forgotPassword
    - POST /resetPassword
    - POST /list
    - POST /<userId>/toggle-activation
    - GET /profile/<userId>
    - PUT /profile/<userId>
    - DELETE /user/<userId>
- blog apis
    - POST /create
    - PUT /<blogId>
    - DELETE /<blogId>
    - GET /all
    - PATCH /<blogId>/state/<state>
    - PATCH /<blogId>/toggleLike
    - GET /search?user=<>&tag=<>&title=<>...
    - GET /<blogId>/likeCount
- comment apis
    - GET /comment/<blogId>
    - POST /comment
    - PUT /<commentId>
    - DELETE /<commentId>
- rating apis
    - GET /rating/<blogId>
    - GET /<blogId>/average
    - POST /rating/<blogId>


### Frontend

#### User portal

#### Admin portal
